//     This file is part of LibreWands.

//    LibreWands is free software: you can redistribute it and/or modify it under the terms of the
//    GNU General Public License as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.

//    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//    the GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License along with LibreWands. If
//   not, see https://www.gnu.org/licenses/.

#include <confuse.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engineCore.c"

// Our current map
uint_fast8_t aMap = 0;

// Our screen size
uint_fast16_t xmax = 0;
uint_fast16_t ymax = 0;

// Our cursor location
uint_fast16_t x = 0;
uint_fast16_t y = 0;

// Our keypress
uint_fast16_t k = 0;

// Our selected tool and tooltip
uint_fast8_t tool = 0;
uint_fast16_t tip = '#';
uint_fast8_t tipColor = 0;
uint_fast8_t tipWalk = 0;

// Animated tiles
char tile_W[8] = {'~', '.', ' ', ' ', ' ', '.', '~', '^'};

// Our mouse events
MEVENT event;

#include "engineMaps.h"

int main()
	{
	// Start our game engine and start a blank map
	initEngine();
	newMap();

	// Run through our animation cycle while we draw our tiles. Get and control our input.
	for (uint_fast8_t A = 0;; A++)
		{
		// Init
		getmaxyx(stdscr, ymax, xmax);
		noecho();
		halfdelay(2);
		clear();

		// For all existing base tiles, draw them.
		for (int C = 0; C < map[aMap].nBase; C++)
			{
			// If this tile is active, unwalkable, invisible and white-on-blue
			if ((map[aMap].Base[C].active) && (map[aMap].Base[C].walk == 0) &&
			    (map[aMap].Base[C].t == ' ') && (map[aMap].Base[C].c == 48))
				{
				// Draw an animated water tile
				attron(COLOR_PAIR(map[aMap].Base[C].c));
				mvprintw(map[aMap].Base[C].y - y, map[aMap].Base[C].x - x, "%c",
					 tile_W[A % 8]);
				attroff(COLOR_PAIR(map[aMap].Base[C].c));
				}
			// If this tile isn't all-of-the-above
			else if (map[aMap].Base[C].active)
				{
				// Draw it normally
				attron(COLOR_PAIR(map[aMap].Base[C].c));
				mvprintw(map[aMap].Base[C].y - y, map[aMap].Base[C].x - x, "%c",
					 map[aMap].Base[C].t);
				attroff(COLOR_PAIR(map[aMap].Base[C].c));
				}
			// If we're hovering over this base tile
			if ((map[aMap].Base[C].x - (xmax / 2) == x) &&
			    (map[aMap].Base[C].y - (ymax / 2) == y))
				{
				// Print out useful info in the bottom-left screen
				mvprintw(ymax - 2, 0, "Walkable: %s\nColor: %i",
					 map[aMap].Base[C].walk ? "True" : "False",
					 map[aMap].Base[C].c);
				}
			}

		// For all existing info tiles, draw them.
		for (int C = 0; C < map[aMap].nInfo; C++)
			{
			// If the given info tile is active
			if (map[aMap].Info[C].active)
				{
				// Draw it with the appropriate character and colour
				attron(COLOR_PAIR(map[aMap].Info[C].c));
				mvprintw(map[aMap].Info[C].y - y, map[aMap].Info[C].x - x, "%c",
					 map[aMap].Info[C].t);
				attroff(COLOR_PAIR(map[aMap].Info[C].c));
				}

			// If we're hovering over the given info tile
			if ((map[aMap].Info[C].x - (xmax / 2) == x) &&
			    (map[aMap].Info[C].y - (ymax / 2) == y))
				{
				// Print out its dialog in the bottom left of the screen
				mvprintw(ymax - 1, 0, "%s", map[aMap].Info[C].dialog);
				}
			}

		// For all existing item tiles, draw them.
		for (int C = 0; C < map[aMap].nItem; C++)
			{
			// If the given item tile is active
			if (map[aMap].Item[C].active)
				{
				// Draw it with the appropriate character and colour
				attron(COLOR_PAIR(map[aMap].Item[C].c));
				mvprintw(map[aMap].Item[C].y - y, map[aMap].Item[C].x - x, "%c",
					 map[aMap].Item[C].t);
				attroff(COLOR_PAIR(map[aMap].Item[C].c));
				}

			// If we're hovering over the given item tile
			if ((map[aMap].Item[C].x - (xmax / 2) == x) &&
			    (map[aMap].Item[C].y - (ymax / 2) == y))
				{
				// Print out its the item ID in the bottom left of the screen
				mvprintw(ymax - 1, 0, "ID#: %i", map[aMap].Item[C].ID);
				}
			}

		// For all existing door tiles, draw them.
		for (int C = 0; C < map[aMap].nDoor; C++)
			{
			// If the given door tile is active
			if (map[aMap].Door[C].active)
				{
				// Draw it with the appropriate character and colour
				attron(COLOR_PAIR(map[aMap].Door[C].c));
				mvprintw(map[aMap].Door[C].y - y, map[aMap].Door[C].x - x, "%c",
					 map[aMap].Door[C].t);
				attroff(COLOR_PAIR(map[aMap].Door[C].c));
				}

			// If we're hovering over the given item tile
			if ((map[aMap].Door[C].x - (xmax / 2) == x) &&
			    (map[aMap].Door[C].y - (ymax / 2) == y))
				{
				// Print out useful info in the bottom-left screen
				mvprintw(ymax - 2, 0, "Status: %s\nMap: %i X: %li Y: %li",
					 map[aMap].Door[C].lock ? "Locked" : "Unlocked",
					 map[aMap].Door[C].ID, map[aMap].Door[C].nx,
					 map[aMap].Door[C].ny);
				}
			}

		// For all existing character tiles, draw them.
		for (int C = 0; C < map[aMap].nChar; C++)
			{
			// If the given char tile is active
			if (map[aMap].Char[C].active)
				{
				// Draw it with the appropriate text character and colour
				attron(COLOR_PAIR(map[aMap].Char[C].c));
				mvprintw(map[aMap].Char[C].y - y, map[aMap].Char[C].x - x, "%c",
					 map[aMap].Char[C].t);
				attroff(COLOR_PAIR(map[aMap].Char[C].c));
				}

			// If we're hovering over the given item tile
			if ((map[aMap].Char[C].x - (xmax / 2) == x) &&
			    (map[aMap].Char[C].y - (ymax / 2) == y))
				{
				// Print out useful info in the bottom-left screen
				mvprintw(ymax - 1, 0, "Name: %s - Type: %s - Moves: %s",
					 map[aMap].Char[C].name,
					 map[aMap].Char[C].type ? "Aggressive" : "Passive",
					 map[aMap].Char[C].move ? "Yes" : "No");
				mvprintw(ymax - 2, 0, "Dialogue 1: %s", map[aMap].Char[C].dialog1);
				mvprintw(ymax - 3, 0, "Dialogue 2: %s", map[aMap].Char[C].dialog2);
				mvprintw(
				    ymax - 4, 0, "Monsters: %i-%i %i-%i %i-%i %i-%i %i-%i",
				    map[aMap].Char[C].mon[0].type, map[aMap].Char[C].mon[0].level,
				    map[aMap].Char[C].mon[1].type, map[aMap].Char[C].mon[1].level,
				    map[aMap].Char[C].mon[2].type, map[aMap].Char[C].mon[2].level,
				    map[aMap].Char[C].mon[3].type, map[aMap].Char[C].mon[3].level,
				    map[aMap].Char[C].mon[4].type, map[aMap].Char[C].mon[4].level);
				}
			}

		// Display coordinates and tool/tips in the top left.
		attron(COLOR_PAIR(0));
		mvprintw(0, 0, "Map: %i X: %li Y: %li", aMap, x, y);
		mvprintw(1, 0, "Tooltip: %c Color: %i Walkable: %s", (char)tip, tipColor,
			 tipWalk ? "True" : "False");

		// Display our cursor based on what tool we're using.
		switch (tool)
			{
			// If continuous painting is off
			case 0:
				mvprintw(ymax / 2, xmax / 2, "!");
				break;
			// If continuous painting is on
			case 1:
				mvprintw(ymax / 2, xmax / 2, "#");
				break;
			}

		// Get and process our user input.
		k = getch();
		if (k == KEY_UP)
			y--;
		if (k == KEY_DOWN)
			y++;
		if (k == KEY_LEFT)
			x--;
		if (k == KEY_RIGHT)
			x++;
		if (k == KEY_F(5))
			saveMap();
		if (k == KEY_F(6))
			loadMap();
		// if(k == KEY_F(7))
		//	copyMap();
		if (k == KEY_F(8))
			// Toggle continuous painting
			tool = !tool;
		if (k == 'b')
			// Change our tooltip
			turnTip();
		if (k == ' ')
			addBase();
		if (tool == 1) // If continuous painting is on
			addBase();
		if (k == 'i')
			addInfo();
		if (k == 'o')
			addItem();
		if (k == 'd')
			addDoor();
		if (k == 'p')
			addChar();
		if (k == KEY_BACKSPACE)
			delTile();
		if ((k == KEY_PPAGE) && (aMap < (nMap - 1)))
			aMap++;
		if ((k == KEY_NPAGE) && (aMap > 0))
			aMap--;
		}

	// Shutdown properly if needed
	endwin();
	return 0;
	}
