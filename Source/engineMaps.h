//     This file is part of LibreWands.

//    LibreWands is free software: you can redistribute it and/or modify it under the terms of the
//    GNU General Public License as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.

//    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//    the GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License along with LibreWands. If
//   not, see https://www.gnu.org/licenses/.

// ---

// For more information on the world maps part of this game engine, see the LibreWands book.

// Monsters that characters sometimes carry. See char below.
struct mapmonster
	{
	uint_fast8_t type;
	uint_fast8_t level;
	};

// Base blocks that reside in the world maps.
struct Base
	{
	uint_fast16_t x;
	uint_fast16_t y;
	uint_fast8_t t;
	uint_fast8_t c;
	uint_fast8_t walk;
	uint_fast8_t active;
	};

// Info signs that reside in the world maps.
struct Info
	{
	uint_fast16_t x;
	uint_fast16_t y;
	uint_fast8_t t;
	uint_fast8_t c;
	char dialog[256];
	uint_fast8_t active;
	};

// Items that reside in the world maps.
struct Item
	{
	uint_fast16_t x;
	uint_fast16_t y;
	uint_fast8_t t;
	uint_fast8_t c;
	uint_fast8_t ID;
	uint_fast8_t active;
	};

// Doors that reside in the world maps.
struct Door
	{
	uint_fast16_t x;
	uint_fast16_t y;
	uint_fast8_t t;
	uint_fast8_t c;
	uint_fast8_t lock;
	uint_fast8_t ID;
	uint_fast16_t nx;
	uint_fast16_t ny;
	uint_fast8_t active;
	};

// Characters that reside in the world maps.
struct Char
	{
	uint_fast16_t x;
	uint_fast16_t y;
	uint_fast8_t t;
	uint_fast8_t c;
	uint_fast8_t type;
	uint_fast8_t move;
	char name[256];
	char dialog1[256];
	char dialog2[256];
	struct mapmonster mon[5];
	uint_fast8_t active;
	};

// Our world maps live here.
struct maps
	{
	struct Item *Item;
	struct Base *Base;
	struct Door *Door;
	struct Info *Info;
	struct Char *Char;
	uint_fast8_t nItem;
	uint_fast8_t nBase;
	uint_fast8_t nDoor;
	uint_fast8_t nInfo;
	uint_fast8_t nChar;
	};
struct maps *map = NULL;
uint_fast8_t nMap = 0;

#include "engineMaps.c"
