# LibreWands

A collaboratively made ncurses RPG featuring magick and monsters. 

## Project Goal

To collaboratively create a unique and high quality ncurses RPG that takes cultural inspiration from the rennaissance and baroque periods in Southern Europe. See the provided documentation for details. 

## Limitations

### Runtime Requirements

* glibc
* ncurses

### Build dependencies

* gcc
* glibc
* ncurses
* texlive-base (optional, for documentation)
* texlive-latex-base (optional, for documentation)

Documentation is installed to the /usr/doc/librewands/ directory.

### Supported Platforms

Currently, the only supported platform is GNU/Linux. Support for other operating systems is planned. 

## Contributing

There are a vast number of ways you can contribute to this project. Any and all help is greatly appreciated. 

* Help design the world using the map editor.
* Help design the world with pencil and paper.
* Help build and tune the game engine.
* Help write the game. 
* Help hunt for bugs. 
* Submit issue reports.
* Edit this README file.
* Edit our Makefile.
* Advocate for better standards compliance.
* Optimise our software.
* Help implement multiplayer.
* Help write our documentation.
* Develop software quality tests.
* Package this software for your favourite operating system. 
* Spread the word about this project. 
* Tell me how I can make contributing easier. 
* Support my efforts at [LiberaPay.](https://liberapay.com/lofenyy/)

## Copyright Notice

     This file is part of LibreWands.

    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/.

