#     This file is part of LibreWands.

#    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

# ---

# Checks for files with missing copyright information

find ../ -type f -not -path '../Build/*' -not -path '../.git/*' -not -name 'LICENSE' | xargs grep -LE "This file is part of LibreWands."

