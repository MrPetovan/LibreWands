#/bin/bash

#     This file is part of LibreWands.

#    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

# ---

# Calculates the percentage of commented lines ofer total lines of code. 
# 20% Appears to be an excellent value. 17.5 is good, and 15 is okay. 

if [ -z "$1" ]
then
ALL=$(cat ../Source/* | wc -l)
COM=$(cat ../Source/* | sed -E 's,\t,,g' | awk '{print substr($0,1,2)}' | sort | uniq -c | grep "//" | grep -oE "[0-9]*")
PER=$(echo "100*($COM / $ALL)" | bc -l)
else
ALL=$(cat "$1" | wc -l)
COM=$(cat "$1" | sed -E 's,\t,,g' | awk '{print substr($0,1,2)}' | sort | uniq -c | grep "//" | grep -oE "[0-9]*")
PER=$(echo "100*($COM / $ALL)" | bc -l)
fi

echo "Comments per line: $COM / $ALL = $PER %"

