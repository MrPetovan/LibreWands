#     This file is part of LibreWands.

#    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

version = 0.0.3

all: editMaps

install:
	mkdir -p /usr/
	mkdir -p /usr/games/
	cp Build/librewands_editmaps /usr/games/librewands_editmaps
	

install-pdf:
	mkdir -p /usr/
	mkdir -p /usr/share/
	mkdir -p /usr/share/doc/
	mkdir -p /usr/share/doc/librewands/
	cp Build/LibreWands.pdf /usr/share/doc/librewands/

uninstall:
	rm /usr/games/librewands_editmaps

uninstall-pdf:
	rm -r /usr/share/doc/librewands/

pdf:
	cd Build/;pdflatex ../Documentation/LibreWands.tex

clean:
	rm -r Build/*

dist:
	mkdir -p "Build/librewands-$(version)"
	cp -r "Assets/" "Build/librewands-$(version)"
	cp -r "Documentation/" "Build/librewands-$(version)"
	cp -r "Source/" "Build/librewands-$(version)"
	cp -r "Tests/" "Build/librewands-$(version)"
	cp -r "Tools/" "Build/librewands-$(version)"
	cp "_clang-format" "Build/librewands-$(version)"
	cp "LICENSE" "Build/librewands-$(version)"
	cp "Makefile" "Build/librewands-$(version)"
	cp "README.md" "Build/librewands-$(version)"
	cd Build/;tar -czf "librewands-$(version).tar.gz" "librewands-$(version)" 

editMaps:
	cd Build/;gcc ../Source/editMaps.c -lncurses -g3 -Og -Wall -fanalyzer -o librewands_editmaps

format:
	clang-format -i Source/*
	clang-format -i Tests/*

